package pop3_test

import (
	"os"
	"testing"

	"git.hansaray.pw/go/pop3"
)

var c *pop3.Client

var (
	Hostname string
	Username string
	Password string
)

func init() {
	Hostname = os.Getenv("POP3_HOSTNAME")
	Username = os.Getenv("POP3_USERNAME")
	Password = os.Getenv("POP3_USERPASS")
}

func TestConnect(t *testing.T) {
	t.Logf("Hostname set to: %s", Hostname)
	if Hostname == "" {
		t.Error("Set env vars!")
		return
	}
	var err error
	if c, err = pop3.DialTLS(Hostname); err != nil {
		t.Fatal(err)
	}
	// Authenticate with the server
	if err = c.Authorization(Username, Password); err != nil {
		t.Fatal(err)
	}

	if err = c.ListCapabilities(); err != nil {
		t.Log(err)
	}

	t.Log("Server Capabilities:")
	t.Logf("CAPA: %v", c.CapaCAPA)
	t.Logf("TOP: %v", c.CapaTOP)
	t.Logf("UIDL: %v", c.CapaUIDL)

	if err = c.Stat(); err != nil {
		t.Fatal(err)
	}
	t.Logf("Count: %v", c.Count)
	t.Logf("Total Size: %v", c.Size)

	if err = c.ListAll(); err != nil {
		t.Fatal(err)
	}

	for _, v := range c.List {
		t.Logf("id: %d UID: %s Size: %d\n", v.ID, v.UID, v.Size)
	}

	if err = c.Quit(); err != nil {
		t.Fatal(err)
	}
}
