[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Go Reference](https://pkg.go.dev/badge/git.hansaray.pw/go/pop3.svg)](https://pkg.go.dev/git.hansaray.pw/go/pop3)

# POP3 Client library

POP3 Client library written in Golang, in accordance to [RFC1939](https://www.ietf.org/rfc/rfc1939.txt) and [RFC2449](https://www.ietf.org/rfc/rfc2449.txt)

Based on [genert](https://github.com/genert/pop3) (Thank you G). I added [RFC2449](https://www.ietf.org/rfc/rfc2449.txt) and removed non standard library packages.

Checks server support for CAPA, TOP, and UIDL in accordance to [RFC2449](https://www.ietf.org/rfc/rfc2449.txt)

Import location:
```go
import "git.hansaray.pw/go/pop3"
```
Hostname should include the port:
```bash
Hostname = pop.excample.com:995
Username = user@example.com
Password = Long password
```
See test for use example.

## Test:

pop_test.go gets the vars from env.
Please set following if you like to run the test:
```go
Hostname = os.Getenv("POP3_HOSTNAME")
Username = os.Getenv("POP3_USERNAME")
Password = os.Getenv("POP3_USERPASS")
```

## Note:

#### Some email servers let you login to a specific folder.

You need to specify the folder in the Username, such as:

	Username = user@excample.com#foldername
	Username = foldername#user@excample.com

More info on folders at [afterlogic.com](https://afterlogic.com/docs/mailbee-net-tutorials/advanced/pop3-folders)
